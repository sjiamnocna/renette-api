# Renette API - React + Nette framework API
- Class for reaching [git@gitlab.com:sjiamnocna/nette-apication.git](https://gitlab.com/sjiamnocna/nette-apication)

- Set for efficient using JSON AJAX requests with Nette Framework and React/Webpack/Node development. With simple Request Authentication

- Proxy pass for local development (query starts with api/ to distinguish local project files)

- REST-like service with as least components and config as possible

- Tracy debugger tool is available if needed, add it as `<script>` to your index.html

- Made for development on Linux as it's the most powerful tool for developers, but should work with Win or Mac with Node

## How to make it work
- Configure service name and key with [git@github.com:sjiamnocna/nette-minimal.git](https://github.com/sjiamnocna/nette-minimal) and redirect all requests to `www/index.php`

- Include Tracy debugger JS script into the `index.html`

# Renette API - React + Nette framework API

- API tool for communication with [git@gitlab.com:sjiamnocna/nette-apication.git](https://gitlab.com/sjiamnocna/nette-apication)

- Made for development on Linux as it's the most helpful for developers

- On Windows, try use WSL2 or Docker (or different) container

- Runs also in Docker 

# Install and get fetching :)