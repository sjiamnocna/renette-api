/** Interfaces can be extended quite simply */

export interface API_fetch_param {
    resource: string
    action: string
    method?: 'GET' | 'POST'
    headers?: Headers
    data?: object
    id?: number
    blocking?: boolean
}

export interface sentMessage {
    type: 'error' | 'warning' | 'success'
    html: string
    recoverable?: true
}

/** These are internal props you probably won't need to extend */

export type credentials = {
    name?: string
    key?: string
}

export type API_config = {
    apiUri: string
    allowedMethod: string[]
    defaultHeaders: Headers
    actionHandlers: {
        [a:string]: Array<actionHandlerFunction<any>>
    }
}

export type API_props = {
    preparing: Promise<any> | false
    authenticated: boolean
    authorized: boolean
    accessKey: string
}

export type actionHandlerFunction<T> = (res: receivedData<T>) =>
    Promise<receivedData<T>>

export type receivedData<t> = {
    action?: string[]
    code?: number
    data: t
}

export type receivedAccessKey = {
    accessKey: string
}