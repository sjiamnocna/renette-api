import { API_props, API_config, API_fetch_param, receivedData, receivedAccessKey, actionHandlerFunction, credentials, } from './types'

class CAPI {
    /**
     * @var bool    Locked for initialization
     * @var string  Access key to use to authenticate each request
     * @var bool    If the API is authorized via key or not
     * @var string  API Base URI
     */
    __props: API_props = {
        /** @var preparing If any blocking call */
        preparing: false,
        /** @var authenticated If API was authenticated with  */
        authenticated: false,
        /** @var authorized If API has authorization to reach protected endpoints */
        authorized: false,
        /** @var accessKey Stored AccessKey for use in authorization headers */
        accessKey: '',
    }

    __config: API_config = {
        apiUri: '/api',
        allowedMethod: ['POST', 'GET'],
        defaultHeaders: new Headers({
            'X-Requested-With': 'XMLHttpRequest',
            'content-type': 'application/json',
        }),
        // default action handlers
        actionHandlers: {
            /**
             * This action sets accessKey and resets key authorization state. It must be the first action invoked by server after new key arrives
             */
            'accessKey': [
                async (res: receivedData<receivedAccessKey>) => {
                    // unauthorize
                    this.__props.authorized = false;
                    // change the access key
                    if (await this.__setAccessKey(res.data.accessKey)){
                        this.__props.authenticated = true;
                    } else {
                        throw Error('Error setting accessKey');
                    }

                    return res;
                },
            ],
            /**
             * With this action, server lettuce know we have authorized access to some protected methods
             * It's checked twice so it's really just informative
             */
            'authorize': [
                async (res: receivedData<receivedAccessKey>) => {
                    // set authorized prop
                    if (!this.isAuthenticated){
                        console.error('Please authenticate first');
                    }
                    
                    this.__props.authorized = true;

                    return res;
                }
            ]
        }
    }

    /** the session is authenticated via access key */
    get isAuthenticated(): boolean {
        return this.__props.authenticated && this.__verifyAccessKey(this.__props.accessKey);
    }

    /** the session is authorized via service name and key */
    get isAuthorized(): boolean {
        return this.__props.authenticated && this.__props.authorized && this.__verifyAccessKey(this.__props.accessKey);
    }

    /** Configuration of the module */
    setConfig(config: Partial<API_config>): void {
        if (Object.keys(config).length === 0) {
            // nothing to change
            return;
        }

        this.__config = {
            ...this.__config,
            ...config,
        };
    }

    setActionHandler<T = any>(action: string, callback: actionHandlerFunction<T>): void {
        if (typeof this.__config.actionHandlers[action] === 'undefined') {
            this.__config.actionHandlers[action] = [];
        }

        this.__config.actionHandlers[action].push(callback);
    }

    /**
     * Allows initialize the request by calling with both name and key
     * 
     * @param props {name: string, key: string}
     */
    constructor(props: credentials){
        const { name, key } = props;
        this.init(name, key);
    }

    async init(serviceName?: string, serviceKey?: string){
        // use init endpoint that allows zero or any number of access credentials
        this.__fetch<receivedAccessKey>({
            resource: 'Chk',
            action: 'init',
            headers: new Headers({
                'X-Service-Name': serviceName ? serviceName : '',
                'X-Service-Key': serviceKey ? serviceKey : '',
            }),
            blocking: true,
        })
        .finally(() => {
            this.__props.preparing = false;
        });
    }

    /**
     * Authenticate session with service name
     * @param function Function after the work is done
     */
    async authenticateWithName(serviceName: string): Promise<void> {
        // must be unique process
        if (this.__props.preparing){
            throw new Error('Another authentication is running, please wait');
        }

        if (this.isAuthenticated){
            throw new Error('API already authenticated');
        }

        // execute Fetch for accessKey and let accessKey action (line 26) handle everything
        this.__fetch<receivedAccessKey>({
            resource: 'Chk',
            action: 'init',
            headers: new Headers({
                'X-Service-Name': serviceName
            }),
            blocking: true,
        })
        .finally(() => {
            this.__props.preparing = false;
        })
    }

    /** Authorizes current service with service key */
    async authorizeWithKey(serviceKey: string): Promise<void> {
        if (this.__props.preparing){
            await this.__props.preparing;
        }
        if (this.isAuthorized) {
            throw new Error('API already authorized');
        }
        if (!this.isAuthenticated) {
            // Not authenticated
            throw new Error('No access key, run `authenticate` first');
        }
        if (!serviceKey) {
            throw new Error('serviceKey must be present');
        }

        this.__fetch<receivedAccessKey>({
            resource: 'Chk',
            action: 'authorize',
            headers: new Headers({
                'X-Access-Key': this.__props.accessKey,
                'X-Service-Key': serviceKey,
            }),
            blocking: true,
        })
        .finally(() => {
            this.__props.preparing = false;
        })
    }

    /**
     * Get request
     * @param param Fetch arguments except data (only POST requests can have data)
     * @returns promise
     */
    async get<T = any>(param: API_fetch_param): Promise<receivedData<T>> {
        param.method = 'GET';

        if (param.data) {
            // cannot use data with GET method
            throw Error('Cant use data with GET method');
        }

        return this.__fetch(param);
    }

    /**
     * Post request
     * @param param Fetch arguments
     * @returns promise
     */
    async post<T = any>(param: API_fetch_param): Promise<receivedData<T>> {
        param.method = 'POST';

        return this.__fetch(param);
    }

    /**
    * @param object Fetch API data
    *  'resource'  string  (reqired)
    *  'action'    string  if not set, default action is used
    *  'id'        int     resource id
    *  'method'    HTTP2   REST method ['GET', 'PUT'] (POST is default)
    *  'headers'   Object  Key => Value pairs
    *  'data'      Object  Key => Value pairs, data used to create request body
    * @param skipWait Should we skip waiting for API authentication? Default false
    * @returns promise
    */
    async __fetch<T>(param: API_fetch_param): Promise<receivedData<T>> {
        // another blocking action is running
        if (this.__props.preparing){
            await this.__props.preparing;
        }

        const { resource, action, id } = param;
        const method: string = param.method && this.__config.allowedMethod.includes(param.method) ? String(param.method) : 'GET';
        const path = `${this.__config.apiUri}/${resource}` + (action ? `/${action}` : '') + (id ? `/${id}` : '');

        const headers = this.__config.defaultHeaders;

        // merge headers
        param.headers && param.headers.forEach((value, name) => {
            headers.set(name, value);
        })

        const fetchParam: RequestInit = {
            method: method,
            headers: headers,
            body: method !== 'GET' ? // if request is GET, body is not allowed
                JSON.stringify(param.data) : null
        }

        const PFetch = fetch(path, fetchParam)
            .then((res) => {
                // @ts-ignore
                if (!res.ok || (res.headers.get('content-type').indexOf('application/json') === -1)) {
                    // return the original response
                    return res;
                }

                // if everything OK and response is JSON
                return res.json();
            })
            .then(async (res: receivedData<any>) => {
                // if any instructions were set or received, process it
                if (res.action && this.__config.actionHandlers) {
                    // array of actions
                    const actions: string[] = res.action ?? [];

                    // use for to avoid bug actions.forEach is not a function
                    for (const action of actions) {
                        res = await this.__execAction<typeof res>(action, res);
                    }
                }
                return res;
            });

            if (param.blocking){
                this.__props.preparing = PFetch;
            }

            return PFetch;
    }

    /**
     * This executes all callbacks attached to certain fetched action
     * 
     * @param actionName    String name of action
     * @param data          ReceivedData<any>
     *  
     * @returns same data as in second argument or modified by actions
     */
    async __execAction<T = any>(actionName: string, data: receivedData<T>): Promise<receivedData<T>> {
        if (actionName in this.__config.actionHandlers) {
            const callables: actionHandlerFunction<T>[] = this.__config.actionHandlers[actionName];

            // execute all
            for (const fn of callables) {
                // allow filter values received from API or completely change data
                data = await fn(data);
            }
        }

        return data;
    }

    /**
     * Verify access key if it's in correct format
     * @param accessKey
     * @returns 
     */
    __verifyAccessKey(accessKey: string): boolean{
        // check access token is in correct JWT format
        return Boolean(accessKey) && typeof accessKey === 'string' && accessKey.split('.').length === 3;
    }

    /**
     * Check access key and set it into the object
     * @param accessKey The access key
     * @returns 
     */
    async __setAccessKey(accessKey: string): Promise<string> {
        if (this.__verifyAccessKey(accessKey)) {
            this.__props.accessKey = accessKey;
            this.__config.defaultHeaders.set('X-Access-Key', accessKey);
            return this.__props.accessKey;
        }
        throw Error('Something went wrong with access key');
    }

    /** Close connection */
    connectionClose(): void {
        this.__props.accessKey = '';
        this.__props.authenticated = false;
        this.__props.authorized = false;
        this.post({
            resource: 'Chk',
            action: 'connectionClose',
        })
    }
}

export default CAPI