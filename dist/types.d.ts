export interface API_fetch_param {
    resource: string;
    action: string;
    method?: 'GET' | 'POST';
    headers?: Headers;
    data?: object;
    id?: number;
    blocking?: boolean;
}
export interface sentMessage {
    type: 'error' | 'warning' | 'success';
    html: string;
    recoverable?: true;
}
export declare type credentials = {
    name?: string;
    key?: string;
};
export declare type API_config = {
    apiUri: string;
    allowedMethod: string[];
    defaultHeaders: Headers;
    actionHandlers: {
        [a: string]: Array<actionHandlerFunction<any>>;
    };
};
export declare type API_props = {
    preparing: Promise<any> | false;
    authenticated: boolean;
    authorized: boolean;
    accessKey: string;
};
export declare type actionHandlerFunction<T> = (res: receivedData<T>) => Promise<receivedData<T>>;
export declare type receivedData<t> = {
    action?: string[];
    code?: number;
    data: t;
};
export declare type receivedAccessKey = {
    accessKey: string;
};
//# sourceMappingURL=types.d.ts.map