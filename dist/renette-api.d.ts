import { API_props, API_config, API_fetch_param, receivedData, actionHandlerFunction, credentials } from './types';
declare class CAPI {
    __props: API_props;
    __config: API_config;
    get isAuthenticated(): boolean;
    get isAuthorized(): boolean;
    setConfig(config: Partial<API_config>): void;
    setActionHandler<T = any>(action: string, callback: actionHandlerFunction<T>): void;
    constructor(props: credentials);
    init(serviceName?: string, serviceKey?: string): Promise<void>;
    authenticateWithName(serviceName: string): Promise<void>;
    authorizeWithKey(serviceKey: string): Promise<void>;
    get<T = any>(param: API_fetch_param): Promise<receivedData<T>>;
    post<T = any>(param: API_fetch_param): Promise<receivedData<T>>;
    __fetch<T>(param: API_fetch_param): Promise<receivedData<T>>;
    __execAction<T = any>(actionName: string, data: receivedData<T>): Promise<receivedData<T>>;
    __verifyAccessKey(accessKey: string): boolean;
    __setAccessKey(accessKey: string): Promise<string>;
    connectionClose(): void;
}
export default CAPI;
//# sourceMappingURL=renette-api.d.ts.map