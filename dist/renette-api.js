class CAPI {
    constructor(props) {
        this.__props = {
            preparing: false,
            authenticated: false,
            authorized: false,
            accessKey: '',
        };
        this.__config = {
            apiUri: '/api',
            allowedMethod: ['POST', 'GET'],
            defaultHeaders: new Headers({
                'X-Requested-With': 'XMLHttpRequest',
                'content-type': 'application/json',
            }),
            actionHandlers: {
                'accessKey': [
                    async (res) => {
                        this.__props.authorized = false;
                        if (await this.__setAccessKey(res.data.accessKey)) {
                            this.__props.authenticated = true;
                        }
                        else {
                            throw Error('Error setting accessKey');
                        }
                        return res;
                    },
                ],
                'authorize': [
                    async (res) => {
                        if (!this.isAuthenticated) {
                            console.error('Please authenticate first');
                        }
                        this.__props.authorized = true;
                        return res;
                    }
                ]
            }
        };
        const { name, key } = props;
        this.init(name, key);
    }
    get isAuthenticated() {
        return this.__props.authenticated && this.__verifyAccessKey(this.__props.accessKey);
    }
    get isAuthorized() {
        return this.__props.authenticated && this.__props.authorized && this.__verifyAccessKey(this.__props.accessKey);
    }
    setConfig(config) {
        if (Object.keys(config).length === 0) {
            return;
        }
        this.__config = {
            ...this.__config,
            ...config,
        };
    }
    setActionHandler(action, callback) {
        if (typeof this.__config.actionHandlers[action] === 'undefined') {
            this.__config.actionHandlers[action] = [];
        }
        this.__config.actionHandlers[action].push(callback);
    }
    async init(serviceName, serviceKey) {
        this.__fetch({
            resource: 'Chk',
            action: 'init',
            headers: new Headers({
                'X-Service-Name': serviceName ? serviceName : '',
                'X-Service-Key': serviceKey ? serviceKey : '',
            }),
            blocking: true,
        })
            .finally(() => {
            this.__props.preparing = false;
        });
    }
    async authenticateWithName(serviceName) {
        if (this.__props.preparing) {
            throw new Error('Another authentication is running, please wait');
        }
        if (this.isAuthenticated) {
            throw new Error('API already authenticated');
        }
        this.__fetch({
            resource: 'Chk',
            action: 'init',
            headers: new Headers({
                'X-Service-Name': serviceName
            }),
            blocking: true,
        })
            .finally(() => {
            this.__props.preparing = false;
        });
    }
    async authorizeWithKey(serviceKey) {
        if (this.__props.preparing) {
            await this.__props.preparing;
        }
        if (this.isAuthorized) {
            throw new Error('API already authorized');
        }
        if (!this.isAuthenticated) {
            throw new Error('No access key, run `authenticate` first');
        }
        if (!serviceKey) {
            throw new Error('serviceKey must be present');
        }
        this.__fetch({
            resource: 'Chk',
            action: 'authorize',
            headers: new Headers({
                'X-Access-Key': this.__props.accessKey,
                'X-Service-Key': serviceKey,
            }),
            blocking: true,
        })
            .finally(() => {
            this.__props.preparing = false;
        });
    }
    async get(param) {
        param.method = 'GET';
        if (param.data) {
            throw Error('Cant use data with GET method');
        }
        return this.__fetch(param);
    }
    async post(param) {
        param.method = 'POST';
        return this.__fetch(param);
    }
    async __fetch(param) {
        if (this.__props.preparing) {
            await this.__props.preparing;
        }
        const { resource, action, id } = param;
        const method = param.method && this.__config.allowedMethod.includes(param.method) ? String(param.method) : 'GET';
        const path = `${this.__config.apiUri}/${resource}` + (action ? `/${action}` : '') + (id ? `/${id}` : '');
        const headers = this.__config.defaultHeaders;
        param.headers && param.headers.forEach((value, name) => {
            headers.set(name, value);
        });
        const fetchParam = {
            method: method,
            headers: headers,
            body: method !== 'GET' ?
                JSON.stringify(param.data) : null
        };
        const PFetch = fetch(path, fetchParam)
            .then((res) => {
            if (!res.ok || (res.headers.get('content-type').indexOf('application/json') === -1)) {
                return res;
            }
            return res.json();
        })
            .then(async (res) => {
            if (res.action && this.__config.actionHandlers) {
                const actions = res.action ?? [];
                for (const action of actions) {
                    res = await this.__execAction(action, res);
                }
            }
            return res;
        });
        if (param.blocking) {
            this.__props.preparing = PFetch;
        }
        return PFetch;
    }
    async __execAction(actionName, data) {
        if (actionName in this.__config.actionHandlers) {
            const callables = this.__config.actionHandlers[actionName];
            for (const fn of callables) {
                data = await fn(data);
            }
        }
        return data;
    }
    __verifyAccessKey(accessKey) {
        return Boolean(accessKey) && typeof accessKey === 'string' && accessKey.split('.').length === 3;
    }
    async __setAccessKey(accessKey) {
        if (this.__verifyAccessKey(accessKey)) {
            this.__props.accessKey = accessKey;
            this.__config.defaultHeaders.set('X-Access-Key', accessKey);
            return this.__props.accessKey;
        }
        throw Error('Something went wrong with access key');
    }
    connectionClose() {
        this.__props.accessKey = '';
        this.__props.authenticated = false;
        this.__props.authorized = false;
        this.post({
            resource: 'Chk',
            action: 'connectionClose',
        });
    }
}
export default CAPI;
